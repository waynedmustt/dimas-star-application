# Dimas Star Application

Welcome to Dimas Star Application Source Code. The App builded as follow: <br />

- Angular 9 as client-application.
- Nest JS as REST-API.
- PostgreSQL as database.
- Docker as development and production environment. <br />

# Development Setup

You need to clone this repo first in your local environment. <br />
After cloned, go to `docker` folder from root project directory, and run: <br />

## docker-compose build 

**Note: Run this command only the first time**

It might take time. After finished, you can continue by run the command: 

## docker-compose up

You can see the docker container logs and .. <br />
App is ready to Rock !

# Run and Develop Application 

## Run and Develop Angular App

Runs the angular app in the development mode by the command: `npm start` in `clients` folder <br />
Open [http://localhost:4500](http://localhost:4500) to view it in the browser. <br />

The page needs to be reloaded when you do edit / develop the app! <br />

## Run and Develop NestJS App

Open [http://localhost:4500/api](http://localhost:4500/api) to view it in the browser.
