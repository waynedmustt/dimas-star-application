#!/bin/bash

if [ ! -d "/data/src/node_modules" ]; then
    cd /data/src
    npm install
fi

if [ ! -d "/data/src/dist/configs" ]; then
    cd /data/src
    npm run copy-configs
fi

npm run start:dev