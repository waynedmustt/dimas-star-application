import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RouterModule, Routes } from '@angular/router';
import { AboutPageComponent } from './about-page/about-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { CareerPageComponent } from './career-page/career-page.component';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';

const appRoutes: Routes = [
  {
    path: 'home',
    children: [
      {path: '', component: HomePageComponent}
    ]
  },
  {
    path: 'about',
    children: [
      {path: '', component: AboutPageComponent}
    ]
  },
  {
    path: 'career',
    children: [
      {path: '', component: CareerPageComponent}
    ]
  },
  {
    path: 'contact',
    children: [
      {path: '', component: ContactPageComponent}
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AboutPageComponent,
    ContactPageComponent,
    CareerPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
    FeatherModule.pick(allIcons)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
