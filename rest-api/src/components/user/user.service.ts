import { Injectable } from '@nestjs/common';
import { Observable, of, from } from 'rxjs';
import { UserRepository } from './user.repository';
import { RoleService } from '../role/role.service';
import { tap, switchMap, mergeMap } from 'rxjs/operators';
import { GlobalUtility } from 'src/utility/global.utility';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private _roleService: RoleService,
  ) {}

  getAll(query?: any, isPromise?: boolean) {
    if (isPromise) {
      return this.userRepository.findByQuery(query);
    }
    return from(this.userRepository.findByQuery(query));
  }

  get(id: number): Observable<any> {
    return from(this.userRepository.findOne(id, {relations: ['role']}));
  }

  create(params: any): Observable<any> {
    return this._roleService.getAll().pipe(
      mergeMap(response => {
        if (!response) {
          return of({
            success: false, 
            message: 'error occurred when retrieved data'
          })
        }

        const roles = response;
        const newParams = params;
        const selectedRole = roles.find(role => newParams.role.type === role.type);
        if (!selectedRole) {
          return of({
            success: false, 
            message: 'error occurred when save data'
          });
        }

        newParams.role.id = selectedRole.id;
        return of(newParams);
      }),
      tap(response => {
        return from(this.userRepository.save(response));
      })
    );
  }

  update(params: any): Observable<any> {
    return of({});
  }
}
