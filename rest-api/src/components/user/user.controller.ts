import { 
  Controller,
  Get,
  Query,
  Param,
  Post,
  Body,
  Put,
  Delete,
  UseFilters,
  UsePipes,
  UseGuards,
  Request
} from '@nestjs/common';
import { User } from 'src/core/models/User';
import { UserService } from './user.service';
import { HttpExceptionFilter } from 'src/utility/http-exception.filter';
import { ValidationPipe } from 'src/core/validations/validation';
import { createUserSchema } from 'src/core/validations/schemas/User';
import { allowedRoles } from 'src/core/decorators/roles.decorator';
import { RolesGuard } from 'src/core/guards/roles.guard';
import { HashPasswordPipe } from 'src/utility/hash-password.pipe';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('user')
@UseGuards(RolesGuard, JwtAuthGuard)
@UseFilters(new HttpExceptionFilter())
export class UserController {
  constructor(
    private readonly userService: UserService
  ) {

  }
  @Get()
  @allowedRoles('admin', 'provider')
  findAll(@Query() query) {
    return this.userService.getAll(query);
  }
  
  @Get(':id')
  @allowedRoles('admin', 'provider')
  findOne(@Param('id') id: number) {
    return this.userService.get(id);
  }

  @Post()
  @allowedRoles('admin')
  @UsePipes(new ValidationPipe(createUserSchema), new HashPasswordPipe())
  create(@Body() body: User) {
    const user = User.getInstance();
    user.init(body);
    return this.userService.create(body);
  }

  @Put(':id')
  @allowedRoles('admin')
  update(@Param('id') id, @Body() body: User) {
    const user = User.getInstance();
    const params: any = user.init(body);
    return this.userService.update(params);
  }

  @Delete(':id')
  @allowedRoles('admin')
  delete(@Param('id') id) {
    return 'delete: user';
  }
}
