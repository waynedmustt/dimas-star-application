import { EntityRepository, Repository } from 'typeorm';
import { Users } from './user.entity';
import { GlobalUtility } from 'src/utility/global.utility';

@EntityRepository(Users)
export class UserRepository extends Repository<Users> {
    private _getConfig() {
        return GlobalUtility.getInstance();
    }

    findByQuery(query: any) {
        if (this._getConfig().isEmptyObject(query)) {
            return this.find({ relations: ['role'] });
        }

        const addedQuery = Object.assign(query, {relations: ['role']})
        return this.find(addedQuery);
    }
}