import {
    PrimaryGeneratedColumn,
    Column,
    Entity,
    OneToOne 
} from 'typeorm';
import { Users } from '../user/user.entity';

@Entity()
export class Roles {
    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    name: string;

    @Column()
    type: string;

    @OneToOne(type => Users, user => user.role, {cascade: ['remove']})
    user: Users;
}