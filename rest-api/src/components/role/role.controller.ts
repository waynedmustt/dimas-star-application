import { 
    Controller,
    UseGuards,
    UseFilters, 
    Get,
    Query,
    Param,
    Post,
    UsePipes,
    Body,
    Put,
    Delete
} from '@nestjs/common';
import { allowedRoles } from 'src/core/decorators/roles.decorator';
import { RoleService } from './role.service';
import { RolesGuard } from 'src/core/guards/roles.guard';
import { HttpExceptionFilter } from 'src/utility/http-exception.filter';
import { HashPasswordPipe } from 'src/utility/hash-password.pipe';
import { Role } from 'src/core/models/Role';
import { ValidationPipe } from 'src/core/validations/validation';
import { createRoleSchema } from 'src/core/validations/schemas/Role';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('role')
@UseGuards(RolesGuard, JwtAuthGuard)
@UseFilters(new HttpExceptionFilter())
export class RoleController {
    constructor(
        private readonly roleService: RoleService
    ) {

    }

    @Get()
    findAll(@Query() query) {
        return this.roleService.getAll(query);
    }
    
    @Get(':id')
    findOne(@Param('id') id: number) {
        return this.roleService.get(id);
    }

    @Post()
    @allowedRoles('admin', 'provider')
    @UsePipes(new ValidationPipe(createRoleSchema), new HashPasswordPipe())
    create(@Body() body: Role) {
        const role = Role.getInstance();
        role.init(body);
        return this.roleService.create(body);
    }

    @Put(':id')
    update(@Param('id') id, @Body() body: Role) {
        const role = Role.getInstance();
        const params: any = role.init(body);
        return this.roleService.update(params);
    }

    @Delete(':id')
    delete(@Param('id') id) {
        return 'delete: role';
    }
}
