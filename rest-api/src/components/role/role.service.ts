import { Injectable } from '@nestjs/common';
import { RoleRepository } from './role.repository';
import { Observable, from, of } from 'rxjs';

@Injectable()
export class RoleService {
    constructor(
        private readonly roleRepository: RoleRepository
    ) {}

    getAll(query?: any): Observable<any> {
        return from(this.roleRepository.findByQuery(query));
    }

    get(id: number): Observable<any> {
        return from(this.roleRepository.findOne(id));
    }

    create(params: any): Observable<any> {
        return from(this.roleRepository.save(params));
    }

    update(params: any): Observable<any> {
        return of({});
    }
}