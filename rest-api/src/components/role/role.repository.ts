import { EntityRepository, Repository } from 'typeorm';
import { Roles } from './role.entity';
import { GlobalUtility } from 'src/utility/global.utility';

@EntityRepository(Roles)
export class RoleRepository extends Repository<Roles> {
    private _getConfig() {
        return GlobalUtility.getInstance();
    }
    
    findByQuery(query: any) {
        if (this._getConfig().isEmptyObject(query)) {
            return this.find();
        }
        return this.find(query);
    }
}