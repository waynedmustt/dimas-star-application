import { GlobalUtility } from "src/utility/global.utility";

const config = GlobalUtility.getInstance().getConfig('config');

export const jwtConstants = {
    secret: config.secret || 'secretKey',
  };