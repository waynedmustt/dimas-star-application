import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';
import { CookieSerializer } from 'src/serialize/cookie.serialize';
import { GlobalUtility } from 'src/utility/global.utility';

const config = GlobalUtility.getInstance().getConfig('config');

@Module({
  imports: [
    UserModule,
    PassportModule.register({ session: true }),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: config.tokenExpired || '60s' }
    })
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy, CookieSerializer],
  exports: [AuthService]
})
export class AuthModule {}
