import { Injectable, Inject } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Observable, of } from 'rxjs';

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService
    ) {}

    async validateUser(username, password) {
      const user: any = await this.userService.getAll(null, true);
      const selectedUser = user.find(user => user.username === username);
      const match = await bcrypt.compare(password, selectedUser.password);
      return match ? user : null;
    }

    login(user: any): Observable<any> {
      const payload = { username: user.username, sub: user.id };
      return of({
        access_token: this.jwtService.sign(payload),
      });
    }
}
