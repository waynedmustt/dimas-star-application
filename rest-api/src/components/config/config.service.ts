import { Injectable } from '@nestjs/common';
import { GlobalUtility } from 'src/utility/global.utility';
import { Observable, of } from 'rxjs';

@Injectable()
export class ConfigService {
    private _utility;
    constructor(
    ) {
        this._utility = GlobalUtility.getInstance();
    }

    find(type: string): Observable<any> {
        let config;
        switch (type) {
            case 'client':
                config = this._utility.getConfig('client-config');
            break;
            case 'server':
                config = this._utility.getConfig('config');
            break;
            default:
                config = this._utility.getConfig('config');
            break;
        }
        return of(config);
    }
}
