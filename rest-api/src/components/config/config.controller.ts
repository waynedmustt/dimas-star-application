import { Controller, Get, Param, UseGuards, UseFilters } from '@nestjs/common';
import { ConfigService } from './config.service';
import { RolesGuard } from 'src/core/guards/roles.guard';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { HttpExceptionFilter } from 'src/utility/http-exception.filter';
import { allowedRoles } from 'src/core/decorators/roles.decorator';

@Controller('config')
@UseGuards(RolesGuard, JwtAuthGuard)
@UseFilters(new HttpExceptionFilter())
export class ConfigController {

    constructor(
        private readonly configService: ConfigService
    ) {
    }

    @Get(':type')
    @allowedRoles('admin')
    find(@Param('type') type: string) {
        return this.configService.find(type);
    }
}
