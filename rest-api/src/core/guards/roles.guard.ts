import {
  Injectable,
  CanActivate, 
  ExecutionContext,
  UnauthorizedException
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';


@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector
  ) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    if (!roles) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const matchRoles = (role, userRoleSession) => {
      return role.indexOf(userRoleSession && userRoleSession.type) !== -1
    };

    if (!request.session || !request.session.passport || !request.session.passport.user) {
      throw new UnauthorizedException();
    }
    const role = request.session.passport.user.role;
    return matchRoles(roles, role);
  }
}