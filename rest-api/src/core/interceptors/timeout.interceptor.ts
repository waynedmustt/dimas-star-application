import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  RequestTimeoutException
} from '@nestjs/common';
import { Observable, TimeoutError, throwError, of } from 'rxjs';
import { timeout, catchError, tap } from 'rxjs/operators';
import { GlobalUtility } from 'src/utility/global.utility';

@Injectable()
export class TimeoutInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const utility = GlobalUtility.getInstance(),
          config = utility.getConfig('config');
    return next
      .handle()
      .pipe(
        timeout(config.requestTimeout || 5000),
        catchError(err => {
          if (err instanceof TimeoutError) {
            return throwError(new RequestTimeoutException());
          }

          return throwError(err);
        }),
        tap(response => {
          if (response === undefined) {
            return of({
              success: false,
              message: 'no response from server'
            });
          }
        })
      );
  }
}