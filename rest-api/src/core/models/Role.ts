import {
	GlobalUtility
} from 'src/utility/global.utility';

export class Role {
	private config: GlobalUtility;

	private static _instance: Role;

	private name: string;
	private type: string;

	static getInstance(): Role {
		if (!Role._instance) {
			Role._instance = new Role();
		}

		return Role._instance;
	}

	init(data) {
		if (!data) {
			return;
		}

		const stringProperties = [
			'name',
			'type',
		];
		
		stringProperties.forEach(property => {
			if (data[property]) {
				this[property] = data[property];
			} 
		});
	}
}