import {
	GlobalUtility
} from 'src/utility/global.utility';

export class User {

	private static _instance: User;

	private username: string;
	private password: string;
	private roleId: string[];
	private firstName: string;
	private lastName: string;
	private isActive: boolean;

	static getInstance(): User {
		if (!User._instance) {
			User._instance = new User();
		}

		return User._instance;
	}

	_getConfig() {
		return GlobalUtility.getInstance();
	}

	init(data) {
		if (!data) {
			return;
		}

		const stringProperties = [
			'username',
			'password',
			'firstName',
			'lastName',
		];
		
		stringProperties.forEach(property => {
			if (data[property]) {
				this[property] = data[property];
			} 
		});

		if (data.isActive) {
			this.isActive = this._getConfig().enforceBoolean(data.isActive);
		}
	}
}