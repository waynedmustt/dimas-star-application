import { SetMetadata } from '@nestjs/common';

export const allowedRoles = (...roles: string[]) => SetMetadata('roles', roles);