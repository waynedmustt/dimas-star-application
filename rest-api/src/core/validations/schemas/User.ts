import joi from '@hapi/joi';

export const createUserSchema = joi.object({
  id: joi.number().optional(),
  firstName: joi.string(),
  lastName: joi.string(),
  username: joi.string()
    .alphanum()
    .min(5)
    .max(20)
    .required(),
  
  password: joi.string()
  .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

  role: joi.any()
    .custom((value, helpers) => {
      if (['admin', 'provider'].indexOf(value && value.type) === -1) {
        return helpers.error('role should be admin or provider');
      }

      return value;
    }, 'custom validation'),

  isActive: joi.boolean()
});