import joi from '@hapi/joi';

export const createRoleSchema = joi.object({
  id: joi.number().optional(),
  name: joi.string(),
  type: joi.any()
    .custom((value, helpers) => {
      if (['admin', 'provider'].indexOf(value && value.type) === -1) {
        return helpers.error('role should be admin or provider');
      }

      return value;
    }, 'custom validation')
});