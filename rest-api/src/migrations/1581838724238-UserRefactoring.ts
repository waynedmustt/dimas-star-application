import {
    MigrationInterface,
    QueryRunner,
    Table,
    TableColumn,
    TableForeignKey
} from 'typeorm';

export class UserRefactoring1581838724238 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`);

        await queryRunner.createTable(new Table({
            name: 'users',
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                    default: `uuid_generate_v4()`
                },
                {
                    name: 'firstName',
                    type: 'varchar'
                },
                {
                    name: 'lastName',
                    type: 'varchar'
                },
                {
                    name: 'username',
                    type: 'varchar'
                },
                {
                    name: 'password',
                    type: 'varchar'
                },
                {
                    name: 'isActive',
                    type: 'boolean'
                }
            ]
        }), true)

        await queryRunner.createTable(new Table({
            name: 'roles',
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                    default: `uuid_generate_v4()`
                },
                {
                    name: 'name',
                    type: 'varchar'
                },
                {
                    name: 'type',
                    type: 'varchar'
                }
            ]
        }), true);

        await queryRunner.addColumn('users', new TableColumn({
            name: 'roleId', 
            type: 'uuid',
            generationStrategy: 'uuid',
            default: `uuid_generate_v4()` 
        }));

        await queryRunner.createForeignKey('users', new TableForeignKey({
            columnNames: ['roleId'],
            referencedColumnNames: ['id'],
            referencedTableName: 'roles',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const user = await queryRunner.getTable('users')
        const foreignKey = user.foreignKeys.find(fk => fk.columnNames.indexOf('roleId') !== -1);
        await queryRunner.dropForeignKey('users', foreignKey);
        await queryRunner.dropColumn('users', 'roleId');
        await queryRunner.dropTable('roles');
        await queryRunner.dropTable('users');
    }

}
