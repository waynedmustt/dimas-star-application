import { Controller, Get, Request, Post, UseGuards, UseInterceptors, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { LocalAuthGuard } from './components/auth/local-auth.guard';
import { AuthService } from './components/auth/auth.service';
import { of } from 'rxjs';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly authService: AuthService
    ) {}

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
