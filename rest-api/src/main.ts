import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { ResponseInterceptor } from './core/interceptors/response.interceptor';
import { TimeoutInterceptor } from './core/interceptors/timeout.interceptor';
import session from 'express-session';
import passport from 'passport';
import { GlobalUtility } from './utility/global.utility';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const config = GlobalUtility.getInstance().getConfig('config');
  app.use(session(
    {
      secret: config.secret || 'secretKey',
      resave: false,
      saveUninitialized: false
    }
  ));
  app.use(passport.initialize());
  app.use(passport.session());
  app.useGlobalInterceptors(new ResponseInterceptor(), new TimeoutInterceptor());
  await app.listen(3000);
}
bootstrap();
