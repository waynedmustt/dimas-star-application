import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './components/user/user.module';
import { logger } from './middleware/logger.middleware';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ResponseInterceptor } from './core/interceptors/response.interceptor';
import { TimeoutInterceptor } from './core/interceptors/timeout.interceptor';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleModule } from './components/role/role.module';
import { AuthModule } from './components/auth/auth.module';
import { ConfigModule } from './components/config/config.module';

@Module({
  imports: [
    AuthModule,
    RoleModule,
    UserModule,
    ConfigModule,
    TypeOrmModule.forRoot()
  ],
  controllers: [AppController],
  providers: [AppService, {
    provide: APP_INTERCEPTOR,
    useClass: ResponseInterceptor
  },
  {
    provide: APP_INTERCEPTOR,
    useClass: TimeoutInterceptor
  }],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(logger)
      .forRoutes('user')
  }
}
