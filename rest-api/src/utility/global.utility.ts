import * as path from 'path';
import * as fs from 'fs';
import * as merge from 'merge';

export class GlobalUtility {
  static _instance: GlobalUtility;

  static getInstance(): GlobalUtility {
    if (!GlobalUtility._instance) {
      return GlobalUtility._instance = new GlobalUtility();
    }

    return GlobalUtility._instance;
  }

  enforceBoolean(data) {
		if (typeof data === 'boolean') {
			return data;
		}

		if (data === 'true') {
			return true;
		}

		if (data === 'false') {
			return false;
		}
  }
  
  enforceArray(data): any[] {
    if (Array.isArray(data)) {
      return data;
    }

    if (typeof data === 'undefined') {
      return [];
    }

    return [data];
  }

  getConfig(configName) {
    const env = process.env.NODE_ENV || 'local', 
          defaultPath = path.resolve(__dirname, '../configs/' + configName + '.json'),
          localPath = path.resolve(__dirname, '../configs/' + configName + '.local.json'),
          envPath = path.resolve(__dirname, '../etc/' + configName + '.' + env + '.json');

    let defaultConfig = require(defaultPath);
    if (fs.existsSync(envPath)) {
      let envConfig = require(envPath);
      return merge.recursive(true, defaultConfig, envConfig);
    }

    if (fs.existsSync(localPath)) {
      let localConfig = require(localPath);
      return merge.recursive(true, defaultConfig, localConfig);
    }

    return defaultConfig;
  }

  isEmptyObject(data) {
    const param = data || {};
    return Object.keys(param).length === 0;
  }
}