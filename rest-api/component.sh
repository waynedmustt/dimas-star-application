#!/bin/bash

if (($# == 0)); then
  echo "Please pass the argument -c <component_name>"
  exit 2
fi

while getopts ":c:" opt; do
  case $opt in
    c)
      echo "creating components: $OPTARG" >&2
      COMPONENT=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ -d "/data/src/src/components/$COMPONENT" ]; then
    echo "Component $COMPONENT is already exist"
    exit 1
fi

cd /data/src/src/components
nest g controller $COMPONENT
nest g module $COMPONENT
nest g service $COMPONENT  
echo "components: $COMPONENT created successfully!"
exit 0